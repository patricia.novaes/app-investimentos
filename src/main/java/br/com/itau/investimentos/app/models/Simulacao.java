package br.com.itau.investimentos.app.models;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idSimulacao;

    @Size(min = 5, max = 100, message = "O nome deve ter entre 5 à 10 caracteres")
    @NotBlank(message = "O nome não pode ser vazio")
    @NotNull(message = "O campo nome não pode ser nulo")
    private String nomeInteressado;

    @Email(message = "Formato do email inválido")
    @NotNull(message = "O campo email não pode ser nulo")
    private String email;

    @NotNull(message = "O campo valor aplicado não pode ser nulo")
    @DecimalMin(value = "100", message = "O valor mínimo para valor aplicado é 100")
    @Column(precision = 10, scale = 2)
    private Double valorAplicado;

    @NotNull(message = "O campo quantidade de meses não pode ser nulo")
    private Integer quantidadeMeses;

    @ManyToOne
    @JoinColumn(name = "investimento_id")
    private Investimento investimento;

    public Simulacao() {
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(Double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public Integer getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(Integer quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }
}

