package br.com.itau.investimentos.app.dtos;

import javax.persistence.Column;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;

public class ResultadoSimulacaoDTO {

    @Column(columnDefinition = "Decimal(3,2)")
    @DecimalMin("0.01")
    private double rendimentoPorMes;

    private String montante;


    public ResultadoSimulacaoDTO() {
    }

    public String getMontante() {
        return montante;
    }

    public void setMontante(String montante) {
        this.montante = montante;
    }

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }
}
