package br.com.itau.investimentos.app.repositories;

import br.com.itau.investimentos.app.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<Investimento, Integer> {


}
