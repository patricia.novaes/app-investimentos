package br.com.itau.investimentos.app.controllers;

import br.com.itau.investimentos.app.dtos.RetornoSimulacaoDTO;
import br.com.itau.investimentos.app.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;


    @GetMapping
    public Iterable<RetornoSimulacaoDTO> exibirTodos(){
        Iterable<RetornoSimulacaoDTO> simulacoes = simulacaoService.buscarTodosSimulacoes();
        return simulacoes;
    }
}
