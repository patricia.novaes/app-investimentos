package br.com.itau.investimentos.app.repositories;

import br.com.itau.investimentos.app.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer> {


}
