package br.com.itau.investimentos.app.services;

import br.com.itau.investimentos.app.dtos.RetornoSimulacaoDTO;
import br.com.itau.investimentos.app.models.Investimento;
import br.com.itau.investimentos.app.dtos.ResultadoSimulacaoDTO;
import br.com.itau.investimentos.app.models.Simulacao;
import br.com.itau.investimentos.app.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    public Simulacao salvarSimulacao(Simulacao simulacao){
        Simulacao simulacaoObjeto = simulacaoRepository.save(simulacao);
        return simulacaoObjeto;
    }

    public Iterable<RetornoSimulacaoDTO> buscarTodosSimulacoes(){
        Iterable<Simulacao> simulacoes = simulacaoRepository.findAll();
        List<RetornoSimulacaoDTO> retornoSimulacaoDTOS = new ArrayList<>();
        for (Simulacao simulacao: simulacoes) {
            RetornoSimulacaoDTO retornoSimulacaoDTO = new RetornoSimulacaoDTO();
            retornoSimulacaoDTO.setEmail(simulacao.getEmail());
            retornoSimulacaoDTO.setNomeInteressado(simulacao.getNomeInteressado());
            retornoSimulacaoDTO.setQuantidadeMeses(simulacao.getQuantidadeMeses());
            retornoSimulacaoDTO.setValorAplicado(simulacao.getValorAplicado());
            retornoSimulacaoDTO.setInvestimentoID(simulacao.getInvestimento().getId());
            retornoSimulacaoDTOS.add(retornoSimulacaoDTO);
        }
        return retornoSimulacaoDTOS;
    }

    public ResultadoSimulacaoDTO realizarSimulacao(Investimento investimento, Simulacao simulacao) {
        ResultadoSimulacaoDTO resultadoSimulacao = new ResultadoSimulacaoDTO();
        //juros simples
        //double calculo =  (simulacao.getValorAplicado() * investimento.getRendimentoAoMes()) / 100;
        //double montante = ((calculo * simulacao.getQuantidadeMeses()) + simulacao.getValorAplicado());

        //juros compostos
        Double juros = investimento.getRendimentoAoMes()/100;
        Double montante = simulacao.getValorAplicado();
        //faz o laço calculando o valor
        for (int tempo = 1; tempo <= simulacao.getQuantidadeMeses(); ++ tempo){
            montante+=(juros*montante);
        }

        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
        decimalFormat.setRoundingMode(RoundingMode.UP);
        resultadoSimulacao.setMontante(decimalFormat.format(montante));
        resultadoSimulacao.setRendimentoPorMes(investimento.getRendimentoAoMes());
        return  resultadoSimulacao;
    }
}
