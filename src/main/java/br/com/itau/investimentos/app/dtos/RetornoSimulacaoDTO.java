package br.com.itau.investimentos.app.dtos;

import br.com.itau.investimentos.app.models.Investimento;

import javax.persistence.*;
import javax.validation.constraints.*;

public class RetornoSimulacaoDTO {

    private String nomeInteressado;

    private String email;

    @Column(precision = 10, scale = 2)
    private Double valorAplicado;

    private Integer quantidadeMeses;

    private Integer investimentoID;

    public RetornoSimulacaoDTO() {

    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(Double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public Integer getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(Integer quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public Integer getInvestimentoID() {
        return investimentoID;
    }

    public void setInvestimentoID(Integer investimentoID) {
        this.investimentoID = investimentoID;
    }
}
