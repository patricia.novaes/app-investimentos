package br.com.itau.investimentos.app.models;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull(message = "O campo nome não pode ser nulo")
    @Column(unique = true)
    private String nome;

    @NotNull(message = "O campo porcentagem de rendimento ao mês não pode ser nulo")
    @Column(columnDefinition = "Decimal(3,2)")
    @DecimalMin("0.01")
    private Double rendimentoAoMes;

    public Investimento() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(Double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }

}
