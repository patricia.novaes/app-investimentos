package br.com.itau.investimentos.app.controllers;


import br.com.itau.investimentos.app.models.Investimento;
import br.com.itau.investimentos.app.dtos.ResultadoSimulacaoDTO;
import br.com.itau.investimentos.app.models.Simulacao;
import br.com.itau.investimentos.app.services.InvestimentoService;
import br.com.itau.investimentos.app.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping
    public ResponseEntity<Investimento> salvarInvestimento(@RequestBody @Valid Investimento investimento){
        Investimento investimentoObjeto = investimentoService.salvarInvestimento(investimento);
        return ResponseEntity.status(200).body(investimentoObjeto);
    }

    @GetMapping
    public Iterable<Investimento> exibirTodos(){
        Iterable<Investimento> investimentos = investimentoService.buscarTodosInvestimentos();
        return investimentos;
    }

    @PostMapping("/{id}/simulacao")
    public ResponseEntity<ResultadoSimulacaoDTO> realizarSimulacao(@PathVariable(name = "id") int id, @RequestBody Simulacao simulacao){
        Investimento investimento = new Investimento();
        try{
            investimento = investimentoService.buscarInvestimento(id);
            ResultadoSimulacaoDTO resultadoSimulacao = simulacaoService.realizarSimulacao(investimento, simulacao);
            simulacao.setInvestimento(investimento);
            simulacaoService.salvarSimulacao(simulacao);
            return ResponseEntity.status(201).body(resultadoSimulacao);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }


}
